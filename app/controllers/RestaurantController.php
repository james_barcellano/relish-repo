<?php


class RestaurantController extends BaseController {

    public function index() {

    }

    public function store() {

        if (Restaurant::findByRestaurantId(Input::get('id'))) {
            return Restaurant::findByRestaurantId(Input::get('id'));
        }

        $restaurant = new Restaurant();
        $input = Input::all();
        $address = '';
        $category = '';

        if (array_key_exists('location', $input)) {
            if(array_key_exists('display_address', $input['location'])) {
                foreach($input['location']['display_address'] as $location) {
                    $address .= $location;
                }
            }
        }

        if(array_key_exists('categories', $input)) {
            $category = $input['categories'][0][0];
        }

        $restaurant->store_id = Input::get('id');
        $restaurant->display_address = $address;
        $restaurant->category = $category;
        $restaurant->fill($input);

        $restaurant->save();

        return $restaurant;
    }

    public function show() {

    }

    public function edit() {

    }

    public function update() {

    }

    public function destroy() {

    }

}