<?php


// Enter the path that the oauth library is in relation to the php file
require_once('lib/OAuth.php');

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/


    // Set your OAuth credentials here
    // These credentials can be obtained from the 'Manage API Access' page in the
    // developers documentation (http://www.yelp.com/developers)
    const CONSUMER_KEY = 'Y1NXhF642gQjtqpABEI0Pg';
    const CONSUMER_SECRET = 'quatk14XTogNrGjIkrkVjHJ9PxI';
    const TOKEN = '_QVA4nsWf0YZ8DZ2QFQ0qovqugvuLWPD';
    const TOKEN_SECRET = '3yqNqjTTRNFJrv_bANlklr5ZpTU';

    const API_HOST = 'api.yelp.com';
    const DEFAULT_TERM = 'lunch';
    const DEFAULT_LOCATION = 'Irvine, CA';
    const SEARCH_LIMIT = 5;
    const SEARCH_PATH = '/v2/search/';
    const BUSINESS_PATH = '/v2/business/';


    public function showWelcome()
	{
		return View::make('hello');
	}

    /**
     * Makes a request to the Yelp API and returns the response
     *
     * @param    $host    The domain host of the API
     * @param    $path    The path of the APi after the domain
     * @return   The JSON response from the request
     */
    function request($host, $path) {
        $unsigned_url = "http://" . $host . $path;

        // Token object built using the OAuth library
        $token = new OAuthToken(self::TOKEN, self::TOKEN_SECRET);

        // Consumer object built using the OAuth library
        $consumer = new OAuthConsumer(self::CONSUMER_KEY, self::CONSUMER_SECRET);

        // Yelp uses HMAC SHA1 encoding
        $signature_method = new OAuthSignatureMethod_HMAC_SHA1();

        $oauthrequest = OAuthRequest::from_consumer_and_token(
            $consumer,
            $token,
            'GET',
            $unsigned_url
        );

        // Sign the request
        $oauthrequest->sign_request($signature_method, $consumer, $token);

        // Get the signed URL
        $signed_url = $oauthrequest->to_url();

        // Send Yelp API Call
        $ch = curl_init($signed_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $data = curl_exec($ch);
        curl_close($ch);

        return $data;
    }

    /**
     * Query the Search API by a search term and location
     *
     * @param    $term        The search term passed to the API
     * @param    $location    The search location passed to the API
     * @return   The JSON response from the request
     */
    function search($term, $location) {
        $url_params = array();

        $url_params['term'] = $term ?: self::DEFAULT_TERM;
        $url_params['location'] = $location?: self::DEFAULT_LOCATION;
        $url_params['limit'] = self::SEARCH_LIMIT;
        $search_path = self::SEARCH_PATH . "?" . http_build_query($url_params);

        return $this->request(self::API_HOST, $search_path);
    }

    /**
     * Query the Business API by business_id
     *
     * @param    $business_id    The ID of the business to query
     * @return   The JSON response from the request
     */
    function get_business($business_id) {
        $business_path = self::BUSINESS_PATH . $business_id;

        return $this->request(self::API_HOST, $business_path);
    }

    /**
     * Queries the API by the input values from the user
     *
     * @param    $term        The search term to query
     * @param    $location    The location of the business to query
     */
    function query_api($term, $location) {
        return print_r($this->search($term, $location));
    }

    public function show() {
//        dd(Input::all());
        /**
         * User input is handled here
         */


        $term = Input::get('term') ?: '';
        $location = Input::get('location') ?: '';

        $this->query_api($term, $location);
    }

}