<?php


class AppointmentController extends BaseController {

    public function index() {

        $getEvents = DB::select(
            DB::raw(
                "
                SELECT * FROM restaurants
                INNER JOIN events
                on restaurants.id = events.restaurant_id
                INNER JOIN users
                on events.user_id = users.id
                order by events_time asc")
        );

        $users = [];

        foreach ($getEvents as $events) {
            $events->events_time = strtotime($events->events_time);

            $events->attenders = DB::select(
                DB::raw(
                    "
                    select users_name, users_image from users
join user_events
on users.id = user_events.user_id
where event_id = $events->id")
            );

            foreach ($events->attenders as $attender) {
                $attender->users_image = base64_encode(file_get_contents($attender->users_image));

            }


        }


        return $getEvents;

//        $appointment = new Appointment;
//        return $appointment->userEvents;
//        return Appointment::all();
    }

    public function store() {

    }

    public function show() {
        $appointment = new Appointment;
        return $appointment->userEvents;
    }

    public function edit() {

    }

    public function update() {

    }

    public function destroy() {

    }

}