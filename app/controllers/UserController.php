<?php


class UserController extends BaseController {

    public function index() {
        return User::all();
    }

    public function store() {

        if (User::findWithEmail(Input::get('users_email'))) {
            return Response::json(array("error" => 409, "message" => "A user with this email address already exists"));
        }

        $user = new User;

//        $user->users_image = base64_encode(file_get_contents("https://lh3.googleusercontent.com/-5PxQ0KbEI5U/AAAAAAAAAAI/AAAAAAAAAAA/Q1MKkpptq08/s120-c/photo.jpg"));

        $user->fill(Input::all());
        $user->save();

        return $user;
    }

    public function show($user_id) {
        return User::find($user_id);
    }

    public function edit($user_id) {

    }

    public function update($user_id) {

    }

    public function destroy($user_id) {

    }

}