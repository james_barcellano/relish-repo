<?php

class Restaurant extends Eloquent {

    protected $table = 'restaurants';

    protected $fillable = array(
        "restaurant_name",
        "restaurant_store_id",
        "restaurant_display_phone",
        "restaurant_image_url",
        "restaurant_rating_img_url",
        "restaurant_category",
        "restaurant_display_address",
        "restaurant_rating"
    );

    public static function findByRestaurantId($restaurantId)
    {
        return self::where("restaurant_store_id", "=", $restaurantId)->first();
    }

}