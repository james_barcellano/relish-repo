<?php

class UserEvent extends Eloquent {

    protected $table = 'user_events';

    protected $fillable = array("event_id", "user_id");


    public function user()
    {
        return $this->belongsTo('User');
    }

    public function event()
    {
        return $this->belongsTo('Appointment');
    }
}