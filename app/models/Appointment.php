<?php

class Appointment extends Eloquent {

    protected $table = 'events';

    protected $fillable = array("restaurant_id", "user_id", "events_time");

    public function userEvents()
    {
        return $this->hasMany('UserEvent');
    }

}
