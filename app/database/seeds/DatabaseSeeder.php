<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		 $this->call('UserTableSeeder');
		 $this->call('RestaurantTableSeeder');
		 $this->call('EventsTableSeeder');
		 $this->call('UserEventTableSeeder');
	}

}

class UserTableSeeder extends Seeder {

    public function run()
    {
        DB::table('users')->delete();

        User::create(
            array(
                'users_email' => 'cameron@weareenvoy.com',
                'users_name' => 'Cameron Berlino',
                'users_image' => public_path() . "/" . "img/cameron.png",
            )
        );

        User::create(
            array(
                'users_email' => 'james@weareenvoy.com',
                'users_name' => 'James Barcellano',
                'users_image' => public_path() . "/" . "img/james.png",
            )
        );

        User::create(
            array(
                'users_email' => 'allison@weareenvoy.com',
                'users_name' => 'Allison Riek',
                'users_image' => public_path() . "/" . "img/allison.png",
            )
        );

        User::create(
            array(
                'users_email' => 'jacob@weareenvoy.com',
                'users_name' => 'Jacob Irwin',
                'users_image' => public_path() . "/" . "img/jacob.png",
            )
        );

        User::create(
            array(
                'users_email' => 'zack@weareenvoy.com',
                'users_name' => 'Zack Travis',
                'users_image' => public_path() . "/" . "img/zack.png",
            )
        );

        User::create(
            array(
                'users_email' => 'craig@weareenvoy.com',
                'users_name' => 'Craig Ormiston',
                'users_image' => public_path() . "/" . "img/craig.png",
            )
        );

        User::create(
            array(
                'users_email' => 'ty@weareenvoy.com',
                'users_name' => 'Ty Lifeset',
                'users_image' => public_path() . "/" . "img/ty.png",
            )
        );

        User::create(
            array(
                'users_email' => 'noname@weareenvoy.com',
                'users_name' => 'John Doe',
                'users_image' => public_path() . "/" . "img/cameron.png",
            )
        );
    }

}

class RestaurantTableSeeder extends Seeder {

    public function run()
    {
        DB::table('restaurants')->delete();

        Restaurant::create(
            array(
                "restaurant_name" => "Pita Pita",
                "restaurant_store_id" => "pita-pita",
                "restaurant_display_phone" => "+1-949-753-0554",
                "restaurant_category" => "Greek",
                "restaurant_display_address" => "Main St.",
                "restaurant_rating" => "3"
            )
        );

        Restaurant::create(
            array(
                "restaurant_name" => "Fukada",
                "restaurant_store_id" => "fukada-irvine",
                "restaurant_display_phone" => "+1-949-222-0554",
                "restaurant_category" => "Japanese",
                "restaurant_display_address" => "8683 Irvine Center Dr, Irvine, CA 92618",
                "restaurant_rating" => "3"
            )
        );

        Restaurant::create(
            array(
                "restaurant_name" => "Chipotle",
                "restaurant_store_id" => "chipotle-irvine",
                "restaurant_display_phone" => "+1-949-753-1231",
                "restaurant_category" => "Mexican",
                "restaurant_display_address" => "81 Fortune Dr #107 Irvine, CA 92618",
                "restaurant_rating" => "4"
            )
        );

        Restaurant::create(
            array(
                "restaurant_name" => "Flame Broiler",
                "restaurant_store_id" => "flame-broiler-irvine",
                "restaurant_display_phone" => "+1-949-450-0114",
                "restaurant_category" => "Korean",
                "restaurant_display_address" => "6630 Irvine Center Dr Irvine, CA 92618",
                "restaurant_rating" => "4"
            )
        );



    }

}

class EventsTableSeeder extends Seeder {

    public function run()
    {
        DB::table('events')->delete();

        Appointment::create(
            array(
                "restaurant_id" => "1",
                "user_id" => "1",
                "events_time" => strtotime("+4 hours 15 min")
            )
        );

        Appointment::create(
            array(
                "restaurant_id" => "2",
                "user_id" => "2",
                "events_time" => strtotime("+ 4 hours 30 min")
            )
        );

        Appointment::create(
            array(
                "restaurant_id" => "3",
                "user_id" => "4",
                "events_time" => strtotime("+ 5 hours 15 min")
            )
        );

        Appointment::create(
            array(
                "restaurant_id" => "4",
                "user_id" => "5",
                "events_time" => strtotime("+ 6 hours 30 min")
            )
        );

        Appointment::create(
            array(
                "restaurant_id" => "2",
                "user_id" => "6",
                "events_time" => strtotime("+ 5 hours 45 min")
            )
        );

        Appointment::create(
            array(
                "restaurant_id" => "1",
                "user_id" => "7",
                "events_time" => strtotime("+ 6 hours 15 min")
            )
        );

    }

}

class UserEventTableSeeder extends Seeder {

    public function run()
    {
        DB::table('user_events')->delete();

        UserEvent::create(array("event_id" => "1", "user_id" => "1"));
        UserEvent::create(array("event_id" => "1", "user_id" => "2"));
        UserEvent::create(array("event_id" => "1", "user_id" => "3"));
        UserEvent::create(array("event_id" => "1", "user_id" => "4"));
        UserEvent::create(array("event_id" => "1", "user_id" => "5"));
        UserEvent::create(array("event_id" => "1", "user_id" => "6"));
        UserEvent::create(array("event_id" => "1", "user_id" => "7"));
        UserEvent::create(array("event_id" => "1", "user_id" => "8"));
        UserEvent::create(array("event_id" => "2", "user_id" => "8"));
        UserEvent::create(array("event_id" => "2", "user_id" => "7"));
        UserEvent::create(array("event_id" => "2", "user_id" => "6"));
        UserEvent::create(array("event_id" => "2", "user_id" => "5"));
        UserEvent::create(array("event_id" => "2", "user_id" => "4"));
        UserEvent::create(array("event_id" => "2", "user_id" => "3"));
        UserEvent::create(array("event_id" => "2", "user_id" => "2"));
        UserEvent::create(array("event_id" => "3", "user_id" => "1"));
        UserEvent::create(array("event_id" => "3", "user_id" => "2"));
        UserEvent::create(array("event_id" => "3", "user_id" => "3"));
        UserEvent::create(array("event_id" => "3", "user_id" => "4"));
        UserEvent::create(array("event_id" => "3", "user_id" => "5"));
        UserEvent::create(array("event_id" => "4", "user_id" => "2"));
        UserEvent::create(array("event_id" => "4", "user_id" => "4"));
        UserEvent::create(array("event_id" => "4", "user_id" => "1"));
        UserEvent::create(array("event_id" => "4", "user_id" => "5"));
        UserEvent::create(array("event_id" => "4", "user_id" => "6"));
        UserEvent::create(array("event_id" => "4", "user_id" => "7"));
        UserEvent::create(array("event_id" => "4", "user_id" => "8"));
        UserEvent::create(array("event_id" => "5", "user_id" => "4"));
        UserEvent::create(array("event_id" => "5", "user_id" => "5"));
        UserEvent::create(array("event_id" => "5", "user_id" => "6"));
        UserEvent::create(array("event_id" => "6", "user_id" => "8"));
        UserEvent::create(array("event_id" => "6", "user_id" => "5"));
    }

}