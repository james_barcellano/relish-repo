<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestaurantsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('restaurants', function(Blueprint $table)
		{
            $table->increments('id');
            $table->string("restaurant_store_id");
            $table->string("restaurant_name");
            $table->string("restaurant_display_phone");
            $table->binary("restaurant_image_url");
            $table->binary("restaurant_rating_img_url");
            $table->string("restaurant_category");
            $table->string("restaurant_display_address");
            $table->integer('restaurant_rating');
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('restaurants');
	}

}
