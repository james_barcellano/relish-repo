<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('events', function(Blueprint $table)
		{
            $table->increments('id');

            $table->integer("restaurant_id")->unsigned()->index();
            $table->integer("user_id")->unsigned()->index();

            $table->foreign("restaurant_id")->references("id")->on("restaurants");
            $table->foreign("user_id")->references("id")->on("users");
            $table->dateTime('events_time');
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('events');
	}

}
